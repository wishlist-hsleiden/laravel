<?php

namespace App\Http\Controllers;

use App\Product;
use App\Wishlist;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if(!$wishlist = Wishlist::find($id)) return response()->json(['error'=>'wishlist was not found'], 404);
        if(!$wishlist->user_id == Auth::id()){
            return response()->json(['error'=>'your do not own this wishlist'], 401);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $input['wishlist_id'] = $id;
        $product = Product::create($input);

        return response()->json(['success' => $product], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$product = Product::find($id)) return response()->json(['error'=>'wishlist was not found'], 404);

        if(!$product->owner == Auth::id()){
            return response()->json(['error'=>'your do not own this wishlist'], 401);
        }
        $product->delete();

        return response()->json(['success'=>'wishlist has been deleted'], $this->successStatus);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$product = Product::find($id)) return response()->json(['error'=>'product was not found'], 404);

        $wishlist = $product->wishlist;
        if(!($wishlist->user_id == Auth::id())){
            return response()->json(['error'=>'your do not own this wishlist'], 401);
        }

        $product->delete();

        return response()->json(['success'=>'product has been deleted'], 200);
    }
}
