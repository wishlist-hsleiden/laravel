<?php

namespace App\Http\Controllers;

use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class WishlistController extends Controller
{
    
    public $successStatus = 200;


    /**
     * Wishlist api
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $wishlists = Wishlist::where('user_id', $user->id)->get();
        return response()->json(['success' => $wishlists], $this->successStatus);
    }

    /**
     * show wishlist for given id
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
        if(!$wishlist = Wishlist::find($id)) return response()->json(['error'=>'wishlist was not found'], 404);

        if (!($request->user('api')->id == $wishlist->user_id || $wishlist->shared == '1')) {
            return response()->json(['error'=>'this wishlist is not yours or is not shared'], 401);
        }

        $products = Wishlist::find($wishlist->id)->products;
            return response()->json(['success' => [
                'wishlist' => $wishlist,
                'products' => $products
            ]], $this->successStatus);
    }

    /**
     * create wishlist
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $input['user_id'] = Auth::id();
        $wishlist = Wishlist::create($input);

        return response()->json(['success'=>$wishlist], $this->successStatus);
    }
    /**
     * update wishlist for given id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update() 
    {

    }

     /**
     * delete wishlist for given id
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!$wishlist = Wishlist::find($id)) return response()->json(['error'=>'wishlist was not found'], 404);

        if(!$wishlist->user_id == Auth::id()){
            return response()->json(['error'=>'your do not own this wishlist'], 401);
        }
        $wishlist->delete();

        return response()->json(['success'=>'wishlist has been deleted'], $this->successStatus);
    }

}
