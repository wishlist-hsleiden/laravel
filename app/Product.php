<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['wishlist_id', 'title', 'description', 'price', 'photo'];

    public function wishlist(){
        return $this->belongsTo('App\Wishlist');
    }
}
