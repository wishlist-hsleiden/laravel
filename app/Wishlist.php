<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{

    protected $fillable = ['user_id', 'title', 'description', 'shared'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products() {
        return $this->hasMany('App\Product');
    }
}
