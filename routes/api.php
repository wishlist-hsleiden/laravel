<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('wishlists/{id}', 'WishlistController@show');

Route::group(['middleware' => 'auth:api'], function(){
	Route::get('me', 'API\UserController@details');
	Route::get('wishlists', 'WishlistController@index');
	Route::post('wishlists', 'WishlistController@store');
	Route::delete('wishlists/{id}', 'WishlistController@destroy');
	Route::post('wishlist/{id}/product', 'ProductController@store');
	Route::delete('product/{id}', 'ProductController@destroy');
});
